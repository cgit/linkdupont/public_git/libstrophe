Name:		libstrophe
Version:	0.9.2
Release:	1%{?dist}
Summary:	A simple, lightweight C library for writing XMPP clients
Group:		System Environment/Libraries

License:	GPLv3 or MIT
URL:		http://strophe.im/libstrophe
Source0:	https://github.com/strophe/%{name}/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:	m4
BuildRequires:	pkgconfig
BuildRequires:	openssl-devel
BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	make
BuildRequires:	libtool
BuildRequires:	expat-devel


%description
libstrophe is a lightweight XMPP client library written in C. It has minimal
dependencies and is configurable for various environments. It runs well on both
Linux, Unix, and Windows based platforms.


%package devel
Summary:	Development files for %{name}
Group:		Development/Libraries
Requires:	%{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for developing
applications that use %{name}.


%prep
%autosetup


%build
./bootstrap.sh
%configure --disable-static
%make_build


%install
rm -rf %{buildroot}
%make_install
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%license LICENSE.txt
%doc README README.markdown GPL-LICENSE.txt MIT-LICENSE.txt
%{_libdir}/%{name}.so.*


%files devel
%license LICENSE.txt
%doc README README.markdown GPL-LICENSE.txt MIT-LICENSE.txt
%{_includedir}/*
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/%{name}.so


%changelog
* Tue May 08 2018 Link Dupont <linkdupont@fedoraproject.org> - 0.9.2-1
- New upstream version

* Sat Oct 15 2016 Link Dupont <linkdupont@fedoraproject.org> - 0.9.1-1
- New upstream release
- Clean up spec file

* Fri Sep 18 2015 Link Dupont <linkdupont@fedoraproject.org> - 0.8.8-1
- Initial package

